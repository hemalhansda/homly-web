import React from 'react';
import './AddMenuItem.css';

import ImageUploader from 'react-images-upload';

class AddMenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isForm: false,
            pictures: [],
            previewImage: null,
            foodName: '',
            foodDetails: '',
            foodCategory: '',
            showCategoryDropdown: false,
            nonVeg: false,
            estimatedTime: 20
        };
    }

    handleAddFoodItem = (event, boolVal) => {
        event.preventDefault();
        event.stopPropagation();
        this.setState({isForm: boolVal});
    };

    handleInput = (event, type) => {
        event.preventDefault();
        event.stopPropagation();
        if (type === 'foodCategory') return;
        this.setState({[type]: event.target.value});
    };

    onDrop = (picture) => {
        this.setState({
            pictures: this.state.pictures.concat(picture),
            previewImage: URL.createObjectURL(picture[0])
        }, () => {
            console.log(this.state);
        });
    };

    showCategoryDropdown = (boolVal) => {
        this.setState({showCategoryDropdown: boolVal});
    };

    updateCategory = (category) => {
        this.setState({foodCategory: category});
    };

    handleToggler = () => {
        this.setState({nonVeg: !this.state.nonVeg});
    }

    handleEstimate = (estimate) => {
        this.setState({estimatedTime: estimate});
    }

    render() {
        return (
            <div className="addmenu-container animate__animated animate__zoomIn">
                {
                    this.state.isForm
                    ? <div className="menu-form">
                        <div className="row">
                            <div className="col-12 food-details-heading">
                                Add Food Details
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-lg-12 col-xl-6 animate__animated animate__fadeInDown">
                                {
                                    this.state.pictures.length
                                    ? <img
                                        className="uploaded-image"
                                        src={this.state.previewImage}
                                        alt="Food" />
                                    : <ImageUploader
                                        withIcon={true}
                                        buttonText='Choose images'
                                        onChange={this.onDrop}
                                        imgExtension={['.jpeg', '.jpg', '.gif', '.png', '.gif']}
                                        maxFileSize={5242880}
                                    />
                                }
                            </div>
                            <div className="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                <div className="row">
                                    <div className="col-sm-6 col-md-6 col-lg-6 col-xl-12 animate__animated animate__fadeInDown">
                                        <input
                                            className="form-input-menu"
                                            name="foodName"
                                            type="text"
                                            placeholder="Enter the name of the food item"
                                            value={this.state.foodName}
                                            onChange={(event) => this.handleInput(event, 'foodName')} />
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-lg-6 col-xl-12 animate__animated animate__fadeInDown">
                                        <textarea
                                            className="form-input-menu-area"
                                            name="foodDetails"
                                            type="text"
                                            placeholder="Enter the details of the food item"
                                            value={this.state.foodDetails}
                                            onChange={(event) => this.handleInput(event, 'foodDetails')} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div
                                        style={{
                                            zIndex: 200
                                        }}
                                        className="col-sm-6 col-md-6 col-lg-6 col-xl-6 modals-exist animate__animated animate__fadeInDown">
                                        <input
                                            className="form-input-menu"
                                            name="foodCategory"
                                            type="text"
                                            onFocus={() => this.showCategoryDropdown(true)}
                                            onBlur={() => this.showCategoryDropdown(false)}
                                            placeholder="Select category"
                                            value={this.state.foodCategory}
                                            style={{
                                                fontSize: '13px',
                                                width: '104%'
                                            }}
                                            onChange={(event) => this.handleInput(event, 'foodCategory')} />
                                        <span className={"material-icons arrow" + 
                                            (this.state.showCategoryDropdown ? ' arrow-up' : '')}>
                                            expand_more
                                        </span>
                                        {
                                            this.state.showCategoryDropdown
                                            && <div className="category-dropdown animate__animated animate__fadeInDown">
                                                <ul type="none" className="category-ul">
                                                    <li onMouseEnter={() => this.updateCategory('STARTER')}>STARTER</li>
                                                    <li onMouseEnter={() => this.updateCategory('MAIN COURSE')}>MAIN COURSE</li>
                                                    <li onMouseEnter={() => this.updateCategory('BEVERAGES')}>BEVERAGES</li>
                                                </ul>
                                            </div>
                                        }
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-lg-6 col-xl-6 animate__animated animate__fadeInDown">
                                        <div
                                            onClick={this.handleToggler}
                                            className="form-input-menu veg-toggler">
                                            <label>VEG</label>
                                            <label>NON-VEG</label>
                                            <span className={this.state.nonVeg ? "switch-on" : ''}></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 animate__animated animate__fadeInDown">
                                        <div className="estimate-picker">
                                            <label>Estimated Time</label>
                                            <span
                                                className={this.state.estimatedTime === 20 ? 'span-active' : ''}
                                                onClick={
                                                    () => this.handleEstimate(20)
                                                }>20'min</span>
                                            <span
                                                className={this.state.estimatedTime === 30 ? 'span-active' : ''}
                                                onClick={
                                                    () => this.handleEstimate(30)
                                                }>30'min</span>
                                            <span
                                                className={this.state.estimatedTime === 40 ? 'span-active' : ''}
                                                onClick={
                                                    () => this.handleEstimate(40)
                                                }>40'min</span>
                                            <span
                                                className={this.state.estimatedTime === 45 ? 'span-active' : ''}
                                                onClick={
                                                    () => this.handleEstimate(45)
                                                }>45'min</span>
                                            <span
                                                className={this.state.estimatedTime === 60 ? 'span-active' : ''}
                                                onClick={
                                                    () => this.handleEstimate(60)
                                                }>60'min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row add-menu-control">
                            <button
                                onClick={(event) => this.handleAddFoodItem(event, false)}
                                className="cancel-button">
                                <span className="material-icons add-item-icon">
                                    arrow_back_ios
                                </span>
                                Back
                            </button>
                            <button className="submit-item-button">
                                <span className="material-icons add-item-icon">
                                    add_box
                                </span>
                                Add Item
                            </button>
                        </div>
                    </div>
                    : <div className="add-item-section">
                        <button
                            onClick={(event) => this.handleAddFoodItem(event, true)}
                            className="add-item-button">
                                <span className="material-icons add-item-icon">
                                    add_box
                                </span>
                                Add Menu Item
                        </button>
                    </div>
                }
            </div>
        );
    }
}

export default AddMenuItem;