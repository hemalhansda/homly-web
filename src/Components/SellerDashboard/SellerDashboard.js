import React from 'react';
import { withRouter } from 'react-router';
import './SellerDashboard.css';

import NavigationBar from '../NavigationBar/NavigationBar';
import AddMenuItem from '../AddMenuItem/AddMenuItem';

class SellerDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                <NavigationBar searchType={"menu item"} />
                <AddMenuItem />
            </>
        );
    }
}

export default withRouter(SellerDashboard);