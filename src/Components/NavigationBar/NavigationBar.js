import React from 'react';
import { Link } from 'react-router-dom';
import './NavigationBar.css';

class NavigationBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <nav className="navbar-container">
                <Link
                    to={'/'}
                    className="navbar-brand">
                    <img
                        className="navbar-logo"
                        src={require('./../../Assets/Images/homly-logo.png').default}
                        alt="homly" />
                </Link>
                <div className="search-container">
                    <span className="material-icons search-icon">
                        search
                    </span>
                    <input
                        className="search-box"
                        type="text"
                        name="searchQuery"
                        placeholder={"Search for " + this.props.searchType}
                        value={this.props.searchQuery} />
                </div>

                <button
                    onClick={() => this.props.showModal('showLogin', true)}
                    className="log-reg-button">Log in</button>
                <button
                    onClick={() => this.props.showModal('showRegister', true)}
                    className="log-reg-button">Sign up</button>
            </nav>
        );
    }
}

export default NavigationBar;