import React from 'react';
import './Menu.css';

import MenuHeader from './MenuHeader/MenuHeader';
import CuisineBar from './CuisineBar/CuisineBar';
import MenuList from './MenuList/MenuList';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 0,
            menuItems: [],
            allMenuItems: [
                {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/104/ef46dfc1654f73838574850f7f4f1104.jpg",
                    "menu_title": "Paneer Butter Masala",
                    "price": "₹165",
                    "menu_category": "main_course"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/104/ef46dfc1654f73838574850f7f4f1104.jpg",
                    "menu_title": "Chicken Butter Masala",
                    "price": "₹265",
                    "menu_category": "main_course"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/104/ef46dfc1654f73838574850f7f4f1104.jpg",
                    "menu_title": "Mutton Butter Masala",
                    "price": "₹465",
                    "menu_category": "main_course"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/f29/b8fc8606043933d0c9c2da3be5b19f29.jpg",
                    "menu_title": "Chilli Fish",
                    "price": "₹120",
                    "menu_category": "starter"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/f29/b8fc8606043933d0c9c2da3be5b19f29.jpg",
                    "menu_title": "Chilli Chicken",
                    "price": "₹220",
                    "menu_category": "starter"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/f29/b8fc8606043933d0c9c2da3be5b19f29.jpg",
                    "menu_title": "Chilli Potato",
                    "price": "₹210",
                    "menu_category": "starter"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/f29/b8fc8606043933d0c9c2da3be5b19f29.jpg",
                    "menu_title": "Chilli Tomato",
                    "price": "₹200",
                    "menu_category": "starter"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/f29/b8fc8606043933d0c9c2da3be5b19f29.jpg",
                    "menu_title": "Chilli Garlic",
                    "price": "₹100",
                    "menu_category": "starter"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/1fe/715ff2c6ed6b30b5b26fbeca40d4c1fe.jpg",
                    "menu_title": "Cold Coffee",
                    "price": "₹90",
                    "menu_category": "beverage"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/1fe/715ff2c6ed6b30b5b26fbeca40d4c1fe.jpg",
                    "menu_title": "Cold Tea",
                    "price": "₹90",
                    "menu_category": "beverage"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/1fe/715ff2c6ed6b30b5b26fbeca40d4c1fe.jpg",
                    "menu_title": "Cold Drink",
                    "price": "₹90",
                    "menu_category": "beverage"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/1fe/715ff2c6ed6b30b5b26fbeca40d4c1fe.jpg",
                    "menu_title": "Coco Cola",
                    "price": "₹50",
                    "menu_category": "beverage"
                }, {
                    "menu_image_url": "https://b.zmtcdn.com/data/dish_photos/1fe/715ff2c6ed6b30b5b26fbeca40d4c1fe.jpg",
                    "menu_title": "Latte",
                    "price": "₹190",
                    "menu_category": "beverage"
                }
            ]
        };
    }

    componentDidMount() {
        let menuItems = this.state.allMenuItems.filter(data => data.menu_category === 'starter');
        this.setState({menuItems});
    }

    tabHandler = (event, newValue) => {
        if (newValue === 0) {
            let menuItems = this.state.allMenuItems.filter(data => data.menu_category === 'starter');
            this.setState({menuItems});
        } else if (newValue === 1) {
            let menuItems = this.state.allMenuItems.filter(data => data.menu_category === 'main_course');
            this.setState({menuItems});
        } else if (newValue === 2) {
            let menuItems = this.state.allMenuItems.filter(data => data.menu_category === 'beverage');
            this.setState({menuItems});
        }
        this.setState({tab: newValue});
    }

    render() {
        return (
            <div className="menu-container">
                <MenuHeader
                    tab={this.state.tab}
                    tabHandler={this.tabHandler}
                />
                <CuisineBar />
                <MenuList
                    tab={this.state.tab}
                    menuItems={this.state.menuItems}
                />
            </div>
        );
    }
}

export default Menu;