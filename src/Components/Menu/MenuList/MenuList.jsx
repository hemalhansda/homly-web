import React from 'react';
import './MenuList.css';

const MenuList = (props) => {
    return (
        <div className="menu-list-container">
            {
                props.menuItems.map((menuData, index) => {
                    return (
                        <div key={index} className="menu-item-container animate__animated animate__fadeInDown">
                            <div className="menu-item-image">
                                <img
                                    src={menuData.menu_image_url}
                                    className="menu-image"
                                    alt={menuData.menu_title} />
                            </div>
                            <div className="menu-item-details">
                                <div className="menu-item-title">
                                    {menuData.menu_title}
                                </div>
                                <div className="menu-item-price">
                                    {menuData.price}
                                </div>
                            </div>
                        </div>
                    );
                })
            }
        </div>
    );
};

export default MenuList;