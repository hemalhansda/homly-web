import React from 'react';
import './MenuHeader.css';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
});

const MenuHeader = (props) => {
    const classes = useStyles();

    return (
        <div className="menu-header">
            <Paper className={classes.root}>
                <Tabs
                    value={props.tab}
                    onChange={props.tabHandler}
                    TabIndicatorProps={{
                        style: {
                            backgroundColor: '#ff8800',
                            color: '#ff8800 !important'
                        }
                    }}
                >
                    <Tab label="Starter" />
                    <Tab label="Main Course" />
                    <Tab label="Beverages" />
                </Tabs>
            </Paper>
            <Link
                to={'/buyer/foodbar'}
                className="back-button-menu">
                <span className="material-icons">
                    arrow_back
                </span>
            </Link>
        </div>
    );
};

export default MenuHeader;