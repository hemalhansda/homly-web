import React from 'react';
import { withRouter } from 'react-router';
import MainNavigator from '../Routes/MainNavigator';
import './Main.css';

import 'animate.css/animate.css';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="main-container">
                <MainNavigator />
            </div>
        )
    }
}

export default Main;