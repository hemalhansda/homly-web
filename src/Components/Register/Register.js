import React from 'react';
import './Register.css';

import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import { Google, Gmail } from '@icons-pack/react-simple-icons';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="register-main-container">
                <div className="register-overlay"></div>
                <div className="register-container animate__animated animate__zoomIn">
                    <div className="register-header">
                        <span className="register-text">Sign up</span>
                        <span
                            onClick={() => this.props.showModal('showRegister', false)}
                            className="register-close-icon">
                            <span class="material-icons">
                                close
                            </span>
                        </span>
                    </div>
                    <div className="register-form">
                        <input
                            className="form-input-style"
                            type="text"
                            name="fullName"
                            placeholder="Full Name"
                            value={this.state.fullName} />
                        <input
                            className="form-input-style"
                            type="email" 
                            name="email" 
                            placeholder="Email" 
                            value={this.state.email} />
                    </div>
                    <div className="register-button-sec">
                        <button
                            className="register-button">
                                Create account
                        </button>
                    </div>
                    <span className="pp-separator-pdp">or</span>
                    <div className="register-google">
                        <button
                            className="register-google-button"
                            >
                            <Google className="icon-right-margin" color="#737373" size={24} />
                            Continue with Google
                        </button>
                    </div>
                    <hr className="hr-register-cus" />
                    <div className="register-footer">
                        <span className="new-user-span">Already have an account?</span>
                        <span className="new-user-span-acc"> Log in</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;