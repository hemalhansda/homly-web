import React from 'react';
import './FoodBoard.css';

import TopKitchens from '../TopKitchens/TopKitchens';
import FoodGrid from '../FoodGrid/FoodGrid';
import Menu from '../Menu/Menu';

import { Route } from 'react-router-dom';

class FoodBoard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                <Route path={'/buyer/foodbar'} component={() =>
                    <div className="foodboard-container">
                        <TopKitchens />
                        <FoodGrid />
                    </div>
                } />
                <Route path={'/buyer/menu'} component={Menu} />
            </>
        );
    }
}

export default FoodBoard;