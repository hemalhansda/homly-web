import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import './OnBoarding.css';

class OnBoarding extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            colorSwitch: true
        };
    }

    render() {
        return (
            <div className="on-boarding-container">
                <Link
                    to={'/buyer/foodbar'}
                    onMouseEnter={() => this.setState({colorSwitch: true})}
                    className={"buyer-part" + (this.state.colorSwitch ? ' color-switch' : '')}>
                    <div className="door-box">
                        <h2 className="type-title">
                            You are
                        </h2>
                        <span className="type-name">hungry?</span>
                    </div>
                </Link>
                <Link
                    to={'/seller'}
                    onMouseEnter={() => this.setState({colorSwitch: false})}
                    className={"seller-part" + (!this.state.colorSwitch ? ' color-switch' : '')}>
                    <div className="door-box">
                        <h2 className="type-title">
                            You are
                        </h2>
                        <span className="type-name">the chef?</span>
                    </div>
                </Link>
            </div>
        );
    }
}

export default withRouter(OnBoarding);