import React from 'react';
import './TopKitchens.css';

import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class TopKitchens extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            imgArr: [
                'https://b.zmtcdn.com/data/brand_creatives/logos/a7051eb7ad394aafd5cd278433c32c30_1611558542.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/7945054647011e72582f29d863967d5e_1621579455.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/fe5db95ae85292933996d4043f600d3b_1611320738.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/d00a24a136119a27c909d7efc1490661_1560948950.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/f19569affdf177676dc916015a0a12a2_1617875309.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/f19569affdf177676dc916015a0a12a2_1617875309.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/fcf2f57ce53caf1bad3fb71abdedaeee_1617920127.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/6a11fd0f30c9fd9ceaff2f5b21f61d23_1617188103.png',
                'https://b.zmtcdn.com/data/brand_creatives/logos/560b209a689eefa9feb367b5d5604097_1611382952.png'
            ],
            settings: {
                dots: false,
                arrows: false,
                speed: 500,
                slidesToShow: 7,
                vertical: false,
                centerMode: false,
                centerPadding: '35px',
                verticalSwiping: false,
                slidesToScroll: 1,
                touchMove: false
            }
        };
    }

    componentDidMount() {
        this.handleSlideSize();
        window.addEventListener("resize", this.handleSlideSize);
    }

    handleSlideSize = () => {
        let slideSize = 7;

        if (window.innerWidth < 430) {
            slideSize = 2;
        } else if (window.innerWidth < 600) {
            slideSize = 3;
        } else if (window.innerWidth < 958) {
            slideSize = 4;
        } else if (window.innerWidth < 990) {
            slideSize = 5;
        } else if (window.innerWidth < 1144) {
            slideSize = 6;
        }

        this.setState((state) => {
            state.settings.slidesToShow = slideSize;
            return state;
        });
    }

    render() {
        return (
            <div className="top-kitchens-container">
                <div className="top-kitchens-header animate__animated animate__slideInDown">Top kitchens in spotlight</div>
                <span
                    onClick={() => this.sliderRef.slickPrev()}
                    className="left-arrow-cc">
                    <span className="material-icons">
                        chevron_left
                    </span>
                </span>
                <span
                    onClick={() => this.sliderRef.slickNext()}
                    className="right-arrow-cc">
                    <span className="material-icons">
                        chevron_right
                    </span>
                </span>
                <Slider
                    className="animate__animated animate__slideInUp"
                    ref={(ref) => this.sliderRef = ref}
                    {...this.state.settings}>
                    {
                        this.state.imgArr.map((imgData, index) => {
                            return (
                                <div key={index + 1} className="kitchen-spot-container">
                                    <img
                                        className="kitchen-spot"
                                        alt="name"
                                        src={imgData} />
                                </div>
                            );
                        })
                    }
                </Slider>
            </div>
        );
    }
}

export default TopKitchens;