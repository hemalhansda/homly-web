import React from 'react';
import { withRouter } from 'react-router';
import FoodBoard from '../FoodBoard/FoodBoard';
import NavigationBar from '../NavigationBar/NavigationBar';
import Register from '../Register/Register';
import Login from '../Login/Login';
import './BuyerDashboard.css';

class BuyerDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLogin: false,
            showRegister: false
        };
    }

    showModal = (type, boolVal) => {
        this.setState({[type]: boolVal});
    }

    render() {
        return (
            <>
                {
                    this.state.showLogin
                        && <Login
                            showModal={this.showModal}
                        />
                }
                {
                    this.state.showRegister
                        && <Register
                            showModal={this.showModal}
                        />
                }
                <NavigationBar
                    showModal={this.showModal}
                    searchType={"kitchen"} />
                <FoodBoard />
            </>
        );
    }
}

export default withRouter(BuyerDashboard);