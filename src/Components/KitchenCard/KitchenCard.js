import React from 'react';
import './KitchenCard.css';

const generateRatings = (ratings) => {
    let jsxData = [];
    for (let i = 0; i < 5; i++) {
        if (i <= ratings) {
            jsxData.push(
                (<span key={i + 1} className="material-icons" style={{color: '#ff8800'}}>
                    star
                </span>)
            );
        } else {
            jsxData.push(
                (<span key={i + 1} className="material-icons" style={{color: '#ff8800'}}>
                    star_border
                </span>)
            );
        }
    }

    return (
        <>
            {
                jsxData
            }
        </>
    );
};

const KitchenCard = (props) => {
    return (
        <div className="kitchen-card">
            <div className="kitchen-image-container">
                <img
                    className="kitchen-image"
                    src={props.foodData.kitchenImage}
                    alt={props.foodData.kitchenName} />
            </div>
            <div className="kitchen-title">
                {
                    props.foodData.kitchenName
                }
            </div>
            <div className="kitchen-rating">
                {
                    generateRatings(props.foodData.ratings)
                }
            </div>
            <div className="kitchen-desc">
                {
                    props.foodData.kitchenDesc
                }
            </div>
            <div className="kitchen-price-time">
                {
                    props.foodData.priceOverview + ' . ' + props.foodData.estimatedTime + ' min'
                }
            </div>
        </div>
    );
};

export default KitchenCard;