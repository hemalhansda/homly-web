import React from 'react';
import './Login.css';

import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import { Google, Gmail } from '@icons-pack/react-simple-icons';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="login-main-container">
                <div className="login-overlay"></div>
                <div className="login-container animate__animated animate__zoomIn">
                    <div className="login-header">
                        <span className="login-text">Log in</span>
                        <span
                            onClick={() => this.props.showModal('showLogin', false)}
                            className="login-close-icon">
                            <span class="material-icons">
                                close
                            </span>
                        </span>
                    </div>
                    <div className="login-phone">
                        <PhoneInput
                            country={'in'}
                            value={this.state.phone}
                            inputProps={{
                                name: 'phone',
                                placeholder: 'Phone Number',
                                required: true,
                                autoFocus: true
                            }}
                            onChange={phone => this.setState({ phone })}
                        />
                    </div>
                    <div className="login-button-sec">
                        <button
                            className="login-button">
                                Send OTP
                        </button>
                    </div>
                    <span className="pp-separator-pdp">or</span>
                    <div className="login-email">
                        <button
                            className="login-email-button"
                            >
                            <Gmail className="icon-right-margin" color="#737373" size={24} />
                            Continue with Email
                        </button>
                    </div>
                    <div className="login-google">
                        <button
                            className="login-google-button"
                            >
                            <Google className="icon-right-margin" color="#737373" size={24} />
                            Continue with Google
                        </button>
                    </div>
                    <hr className="hr-login-cus" />
                    <div className="login-footer">
                        <span className="new-user-span">New to homly?</span>
                        <span className="new-user-span-acc"> Create account</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;