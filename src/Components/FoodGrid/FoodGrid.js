import React from 'react';
import KitchenCard from '../KitchenCard/KitchenCard';
import './FoodGrid.css';

import { Link } from 'react-router-dom';

class FoodGrid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            foodList: [
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                },
                {
                    kitchenName: 'Day n Night',
                    ratings: 4,
                    kitchenImage: 'https://b.zmtcdn.com/data/pictures/chains/9/18573069/67789d9017e7bc396cc340736e936701_o2_featured_v2.jpg',
                    kitchenDesc: 'Fast Food, Burger',
                    priceOverview: '₹100 for one',
                    estimatedTime: 31
                }
            ]
        };
    }

    render() {
        return (
            <div className="foodgrid-container">
                <div className="foodgrid-header animate__animated animate__slideInDown">Best Food near You</div>
                <div className="container animate__animated animate__slideInUp">
                    <div className="row">
                        {
                            this.state.foodList.map((foodData, index) => {
                                return (
                                    <Link
                                        key={index + 1}
                                        to={'/buyer/menu'}
                                        className="col-sm" style={{
                                            margin: '0px',
                                            padding: '0px'
                                        }}>
                                        <KitchenCard
                                            foodData={foodData}
                                        />
                                    </Link>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default FoodGrid;