import React from 'react';
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom';
import BuyerDashboard from '../Components/BuyerDashboard/BuyerDashboard';
import OnBoarding from '../Components/OnBoarding/OnBoarding';
import SellerDashboard from '../Components/SellerDashboard/SellerDashboard';

import { ToastContainer } from 'react-toastify';

class MainNavigator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return(
            <>
                <BrowserRouter>
                    <Switch>
                        <Route path={'/'} component={OnBoarding} exact />
                        <Route path={'/seller'} component={SellerDashboard} />
                        <Route path={'/buyer'} component={BuyerDashboard} />
                    </Switch>
                    <ToastContainer autoClose={2000} />
                </BrowserRouter>
            </>
        );
    }
}

export default MainNavigator;